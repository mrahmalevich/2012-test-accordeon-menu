//
//  AbstractJSONModel.h
//  

#import <Foundation/Foundation.h>
#import "SBJSON.h"

@interface AbstractJSONResponse : NSObject {
	NSMutableDictionary *parsedDictionary;
    NSMutableDictionary *responseDictionary;
	NSError *parseError;
	NSError *apiError;
    NSMutableDictionary *errorDict;
}

@property (nonatomic, retain) NSMutableDictionary *parsedDictionary;
@property (nonatomic, retain) NSMutableDictionary *responseDictionary;
@property (nonatomic, retain) NSError *parseError;
@property (nonatomic, retain) NSMutableDictionary *errorDict;
@property (nonatomic, retain) NSError *apiError;

- (id)initWithJSON:(NSString *)jsonString;
- (id)getParsedValue:(NSString *)key;

@end
