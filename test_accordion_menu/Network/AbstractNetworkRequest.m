//
//  AbstractNetworkRequest.m
//

#import "AbstractNetworkRequest.h"
#import "ASIDownloadCache.h"

#define TimeOutSeconds 20
#define NumberOfTimesToRetryOnTimeout 3
#define CACHESTALETIME 5*60

@implementation AbstractNetworkRequest

@synthesize request, requestURL, requestResponse, delegate, usesAsyncRequest, dontUseCache;

#pragma mark - class methods
+ (NSString *)getGETUrlWithBase:(NSString *)base andParams:(NSDictionary *)params
{
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@?", base];
    NSMutableArray *strings = [NSMutableArray arrayWithCapacity:[params count]];
    for (NSString *key in params) {
        NSString *value = [params objectForKey:key];
        [strings addObject:[NSString stringWithFormat:@"%@=%@", key, value]];
    }
    [urlString appendString:[strings componentsJoinedByString:@"&"]];
    return urlString;
}

#pragma mark - memory managment
- (id) init 
{
    self = [super init];
    if (self) {
        self.usesAsyncRequest = NO;
        self.requestURL = @"";
        self.dontUseCache = YES;
    }
    return self;
}

- (id) initWithDelegate:(id<AsyncRequestDelegate>)owner
{
    self = [self init];
    if (self) {
        self.delegate = owner;
    }
    return self;
}

- (void)dealloc
{
    [requestURL release];
    [request release];
    [requestResponse release];
    [super dealloc];
}

#pragma mark - request lifecycle
- (void) initRequest 
{
    NSString *url = [NSString stringWithString:requestURL];
    if ([url length] == 0) {
        return;
    }
	
    self.request = [[[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:url]] autorelease];    
    [request setDelegate:nil];
    [request setValidatesSecureCertificate:NO];
    [request setTimeOutSeconds:TimeOutSeconds];
    [request setNumberOfTimesToRetryOnTimeout:NumberOfTimesToRetryOnTimeout];
    
    [[ASIDownloadCache sharedCache] setShouldRespectCacheControlHeaders:NO]; 
    [request setDownloadCache:[ASIDownloadCache sharedCache]];
  
    if (dontUseCache) {
        [request setCachePolicy:ASIDoNotReadFromCacheCachePolicy];
    } else {
        [request setSecondsToCache:CACHESTALETIME];
        [request setCachePolicy:ASIAskServerIfModifiedWhenStaleCachePolicy | ASIFallbackToCacheIfLoadFailsCachePolicy];
    }
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
}

- (void) sendRequest 
{
    [self initRequest];

    if (usesAsyncRequest) {
        [request setDelegate:self];
        [request setCompletionBlock:^ {
            self.requestResponse = [request responseString];
            if ([delegate respondsToSelector:@selector(objectReady:)]) 
                [delegate objectReady:self];
        }];
        [request setFailedBlock:^ {
            NSError *err = [request error];
            self.requestResponse = [err localizedDescription];
            if ([delegate respondsToSelector:@selector(objectFail:)]) {
                [delegate objectFail:self];
            }
        }];
        [request startAsynchronous];
    } else {
        [request startSynchronous];
        NSError *err = [request error];
        if (err) {
            self.requestResponse = [err localizedDescription];
        } else {
            self.requestResponse = [request responseString];
        }
    }
}

- (id) toObject 
{
    return self;
}

@end
