//
//  AbstractNetworkRequest.h
//  

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "ASIDownloadCache.h"

@protocol AsyncRequestDelegate <NSObject>
@optional
- (void)objectReady:(id)object;
- (void)objectFail:(id)object;
@end

@interface AbstractNetworkRequest : NSObject {
    ASIFormDataRequest *request;
    NSString *requestURL;
    NSString *requestResponse;
    BOOL usesAsyncRequest;
    BOOL dontUseCache;
    id<AsyncRequestDelegate> delegate;
}

@property (nonatomic, copy) NSString *requestURL;
@property (nonatomic, copy) NSString *requestResponse;
@property (nonatomic, retain) ASIFormDataRequest *request;
@property (nonatomic, assign) id<AsyncRequestDelegate> delegate;
@property (nonatomic) BOOL usesAsyncRequest;
@property (nonatomic) BOOL dontUseCache;

+ (NSString *)getGETUrlWithBase:(NSString *)base andParams:(NSDictionary *)params;

- (id) init;
- (id) initWithDelegate:(id<AsyncRequestDelegate>)owner;
- (void) initRequest;
- (void) sendRequest;
- (id) toObject;

@end
