//
//  DataManager.m
//  test_accordion_menu
//
//  Created by Михаил Рахмалевич on 09.08.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import "DataManager.h"
#import "GooglePlacesRequest.h"
#import "AbstractJSONResponse.h"

#define gpLodging       @"lodging"
#define gpCarRental     @"car_rental"
#define gpRestaurant    @"restaurant"
#define gpStore         @"store"

static DataManager *_sharedInstance = nil;

@interface DataManager (Private)
- (void)insertPlaces:(NSArray *)placesDictsArray withType:(NSString *)type;
@end

@implementation DataManager
@synthesize managedObjectModel = __managedObjectModel;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

#pragma mark - memory managment
+ (DataManager*)sharedInstance
{
    if (_sharedInstance == nil) {
        _sharedInstance = [[self alloc] init];
    }
    return _sharedInstance;
}

- (void)dealloc
{
    [__managedObjectContext release];
    [__managedObjectModel release];
    [__persistentStoreCoordinator release];
    [super dealloc];
}

#pragma mark - core data stack
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
        [__managedObjectContext setUndoManager:nil];
    }
    return __managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"placesdb" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];   
    return __managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{  
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - loading
- (void)loadPlacesData
{
    [self sendPlacesRequestForType:gpCarRental];
    [self sendPlacesRequestForType:gpLodging];
    [self sendPlacesRequestForType:gpRestaurant];
    [self sendPlacesRequestForType:gpStore];
}

- (void)sendPlacesRequestForType:(NSString *)type
{
    GooglePlacesRequest *googleRequest = [[GooglePlacesRequest alloc] initWithDelegate:self];
    googleRequest.type = type;
    [googleRequest sendRequest];
}

- (void)insertPlaces:(NSArray *)placesDictsArray withType:(NSString *)type
{
    NSManagedObjectContext *context = __managedObjectContext;
    
    NSError *error = nil;
    for (NSDictionary *dict in placesDictsArray) {
        Place *place = [NSEntityDescription insertNewObjectForEntityForName:@"Place" inManagedObjectContext:context];
        place.type = type;
        place.name = [dict valueForKey:@"name"];
        place.placeId = [dict valueForKey:@"id"];
        place.vicinity = [dict valueForKey:@"vicinity"];
        place.latitude = [NSNumber numberWithDouble:[[dict valueForKeyPath:@"geometry.location.lat"] doubleValue]];
        place.longitude = [NSNumber numberWithDouble:[[dict valueForKeyPath:@"geometry.location.lng"] doubleValue]];
        place.rating = [NSNumber numberWithDouble:[[dict valueForKey:@"rating"] doubleValue]];
    }
    [context save:&error];
    
    if (error) {
        NSLog(@"ERROR DURING SAVING CONTEXT: %@", [error localizedDescription]);
    }
}

#pragma mark - async request delegate
- (void)objectReady:(id)object
{
    if ([object isKindOfClass:[GooglePlacesRequest class]]) {
        AbstractJSONResponse *response = [[object toObject] retain];
        if ([[response getParsedValue:@"status"] isEqualToString:@"OK"]) {
            NSString *type = [(GooglePlacesRequest *)object type];
            NSArray *placesDictsArray = [response getParsedValue:@"results"];
            [self insertPlaces:placesDictsArray withType:type];
        }
        [response autorelease];
    }
    
    [object autorelease];
}

- (void)objectFail:(id)object
{
    [object autorelease];
}

#pragma mark - utils
- (NSFetchedResultsController *)placesControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Place" inManagedObjectContext:__managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSSortDescriptor *sortDescriptorType = [[NSSortDescriptor alloc] initWithKey:@"type" ascending:YES];
    NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObjects:sortDescriptorType, sortDescriptorName, nil]];
    [sortDescriptorType release];
    [sortDescriptorName release];
    
    NSFetchedResultsController *controller = [[[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:__managedObjectContext sectionNameKeyPath:@"type" cacheName:nil] autorelease];
    [request release];
    
    [controller setDelegate:delegate];
    
    NSError *error = nil;
    [controller performFetch:&error]; 
    
    if (error) {
        NSLog(@"ERROR DURING FETCH: %@", [error localizedDescription]);
        return nil;
    } else {
        return controller;
    }
}

@end
