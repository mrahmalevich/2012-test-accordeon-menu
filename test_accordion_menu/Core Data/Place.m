//
//  Place.m
//  test_accordion_menu
//
//  Created by Михаил Рахмалевич on 09.08.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import "Place.h"


@implementation Place

@dynamic placeId;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic rating;
@dynamic type;
@dynamic vicinity;

@end
