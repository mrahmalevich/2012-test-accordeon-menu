//
//  ViewController.h
//  test_accordion_menu
//
//  Created by Михаил Рахмалевич on 09.08.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
